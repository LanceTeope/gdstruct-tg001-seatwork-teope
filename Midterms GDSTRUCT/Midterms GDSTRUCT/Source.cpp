#include <string>
#include <iostream>
#include <vector>
#include <time.h>
#include <conio.h>
#include "UnorderedArray.h"

using namespace std;

int main()
{
	srand(time(NULL));
	int size, del, find;
	cout << "What is the size of the array: " << endl;
	cin >> size;

	UnorderedArray<int> numbers(size);
	for (int i = 1; i <= size; i++)
	{
		int val = rand() % 100 + 1;
		numbers.push(val);
	}

	cout << "Unordered Array: " << endl;
	for (int i = 0; i < numbers.getSize(); i++)
	{
		cout << numbers[i] << "	-	[" << i << "]" << endl;
	}
	cout << "=================================" << endl << endl;
	cout << "What number would you like to delete? " << endl;
	cin >> del;
	numbers.remove(del);
	system("cls");

	cout << "Unordered Array: " << endl;
	for (int i = 0; i < numbers.getSize(); i++)
	{
		cout << numbers[i] << "	-	[" << i << "]" << endl;
	}

	cout << "What number would you like to find? " << endl;
	cin >> find;
	numbers.search(find);

	_getch();
}